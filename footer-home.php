<?php 

global $echo_option;


?>

</div>
	<footer>
		<div class="footer__content">
			<div class="footer__widgets_content">
				<?php dynamic_sidebar('sidebar-footer'); ?>
			</div>
				<div class="footer-content__setting">
					<?php if($echo_option['footer-copy'] != '0'): ?>
					<p><?php echo esc_html($echo_option['footer-copy']); ?></p>
				<?php endif; ?>
				</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
<script>
	new WOW().init();
</script>
<script>
	jQuery(document).ready(function($) {
		$("#btn__media").click(function(){
		$(".menu-nav-top-container").toggle();
		});
	});
</script>
</body>
</html>