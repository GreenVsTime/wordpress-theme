<!doctype html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>
			<div class="header__conten_nav">
				<button id="btn__media" class="show__nav">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button>
				<?php
						$args = array('theme_location'  => 'menu-1',
						'container'       => 'nav',
						'menu_id'         => 'top__nav', 
						'menu_class'      => 'nav header__nav',
						);
					 wp_nav_menu($args); ?>
			</div>
		</header>
	<div class="wrapper">
