<?php 

global $echo_option;


?>

</div>
	<footer>
		<div class="footer__content">
			<? if (is_active_sidebar('sidebar-footer') ) {?>
			<div class="footer__widgets_content">
					<?php dynamic_sidebar('sidebar-footer'); ?>
			</div>
			<?php } ?>
			<div class="footer-content__setting">
					<?php if($echo_option['footer-copy'] != '0'): ?>
					<p><?php echo esc_html($echo_option['footer-copy']); ?></p>
				<?php endif; ?>
				<div class="footer-content__social">
					<p>
						<a href="<?php echo esc_html($echo_option['socials-sp_contacts']['Twitter']);?>" class="one footer-social__button twitter">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
						<a href="<?php echo esc_html($echo_option['socials-sp_contacts']['linkedin']);?>" class="footer-social__button linked">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						</a>
						<a href="<?php echo esc_html($echo_option['socials-sp_contacts']['Google+']);?>" class="footer-social__button google">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
						</a>
						<a href="<?php echo esc_html($echo_option['socials-sp_contacts']['Facebook']);?>" class="footer-social__button facebook">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
					</p>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
<script>
	new WOW().init();
</script>
<script>
	jQuery(document).ready(function($) {
		$("#btn__media").click(function(){
		$(".menu-nav-top-container").toggle();
		});
	});
</script>
<script>
jQuery(document).ready(function($) {
	var selectedClass = "";
	$(".nav__link").click(function(){ 
		selectedClass = $(this).attr("data-rel"); 
			  $("#products__portfolio").fadeTo(100, 0.1);
		$(".products__portfolio-items").not("."+selectedClass).fadeOut().removeClass('scale-anm');
			 setTimeout(function() {
			   $("."+selectedClass).fadeIn().addClass('scale-anm');
			   $("#products__portfolio ").fadeTo(300, 1);
			 }, 300); 
		
	});
});
</script>
</body>
</html>