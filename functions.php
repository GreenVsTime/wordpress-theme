<?php

/**Style***/
function enqueue_styles() {
	wp_enqueue_style( 'whitesquare-style', get_stylesheet_uri());
	wp_register_style('font-style', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,800');
	wp_enqueue_style( 'font-style');
	wp_enqueue_style( 'echo-style', get_template_directory_uri() . "/style.css",array());
	wp_enqueue_style( 'style', get_template_directory_uri() . "/style/style.css");
	wp_enqueue_style( 'sprites', get_template_directory_uri() . "/style/sprites.css");
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . "/style/font-awesome.min.css");
	wp_enqueue_style( 'animate', get_template_directory_uri() . "/style/animate.css");
	wp_enqueue_style( 'media', get_template_directory_uri() . "/style/media.css");
}
add_action('wp_enqueue_scripts', 'enqueue_styles');
/*register menu*/
register_nav_menus( array(
	'menu-1' => esc_html__('Primary', 'echo'),
) );
/*add scripts*/
function enqueue_scripts () {
	wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
	wp_enqueue_script('jquery');
	wp_enqueue_script('wow', get_template_directory_uri() . "/js/wow.min.js" );
	wp_enqueue_script('parallax', get_template_directory_uri() . "/js/parallax.min.js" );
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}
/***Add widgets in footer***/
function echo_widgets_init(){
	register_sidebar(array(
	'name' => esc_html__('Footer sidebar', 'echo'),
	'id' => 'sidebar-footer',
	'description' => esc_html__('Footer area', 'echo'),
	'before_widget' => '<div id="%1$s" class="%2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
	)
	);
}
add_action('widgets_init','echo_widgets_init');
/* Init qoob libs */
add_filter( 'qoob_libs', 'echo_add_theme_lib', 10, 2 );

if ( ! function_exists( 'echo_add_theme_lib' ) ) {
  /**
   * Adding lib in qoob libs
   *
   * @param array $qoob_libs Array with url to qoob lib.
   */
  function echo_add_theme_lib( $qoob_libs ) {
    array_push( $qoob_libs, get_template_directory() . '/blocks/lib.json' );
    return $qoob_libs;
  }
}
/***ADD Class Active nav menu***/
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'nav__link-active';
    }
    return $classes;
}


/***TMG Class***/
require get_template_directory() . '/inc/init-tgm.php';
/***Them options***/
require get_template_directory() . '/inc/theme-options.php';